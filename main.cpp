#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

using namespace std;
using namespace boost;

typedef int (*Puzzle)(const string&, int part);

vector<int>     getIntegersInFile(const string& fName);
vector<string>  getLinesInFile(const string& fName);
vector<string>  splitLine(const string& line, char splitter);

//The days
int day_1(const string& fName, int part);
int day_2(const string& fName, int part);

int main()
{
    try
    {
        //Useful stuff
        int day(2);

        //Assign function
        Puzzle puzzle;
        puzzle = &(day_2);
        string dataFolder("C:\\AllenProjects\\advent-of-code-2020");

        //Create input file name
        stringstream inputFile;
        inputFile << dataFolder << "\\Dec-" << day <<".txt";

        int part(2);
        int res = puzzle(inputFile.str(), part);
        cout << "Todays answer is: " << res;
        return 0;
    }
    catch(...)
    {
        cerr << "Something got screwded up..";
    }
}

int day_1(const string& fName, int part)
{
    vector<int> inputData(getIntegersInFile(fName));
    int sum(2020);

    if(part == 1)
    {
        //Find one pair that adds up to 2020, and return the pairs product
        for(int b = 0; b < inputData.size() -2; b++)
            for(int e = inputData.size() -1; e > 1; e--)
            {
                int test_sum = inputData[b] + inputData[e];
                if(test_sum == sum)
                {
                    return inputData[b] * inputData[e];
                }
            }
    }
    else if(part == 2)
    {
        vector<int>::iterator third = inputData.begin();
        for(int b = 0; b < inputData.size() -2; b++)
            for(int e = inputData.size() -1; e > 1; e--)
                for(int t = 0; t < inputData.size(); t++)
                {
                    if(t != b && t != e)
                    {
                        int test_sum = inputData[b] + inputData[e] + third[t];
                        if(test_sum == sum)
                        {
                            //cout << "Got the numbers: " << inputData[b] <<", " << inputData[e]<<", " <<  third[t];
                            return inputData[b] * inputData[e] * inputData[t];
                        }
                    }
                }
    }
    return -1;
}

int day_2(const string& fName, int part)
{
    vector<string> sv = getLinesInFile(fName);
    vector<string>::iterator it(sv.begin());
    cout << "Got " << sv.size() << " lines";

    //find number of valid passwords
    int nrOfValidPasswords(0);

    while(it != sv.end())
    {
        string theLine(*it);
        vector<string> parsedLine;
        split(parsedLine, theLine, is_any_of(": -"), token_compress_on);
        int     policyMin(stoi( parsedLine[0]));
        int     policyMax(stoi( parsedLine[1]));
        char    theChar(        parsedLine[2][0]);
        string  pword(          parsedLine[3]);

        if(part == 1)
        {
            int charCount(count(pword.begin(), pword.end(), theChar));
            nrOfValidPasswords += (charCount >= policyMin && charCount <= policyMax) ? 1 : 0;
        }
        else if(part == 2)
        {
            //PolicyMin and max are positions
            pair<int,int> p1(policyMin - 1, 0);
            pair<int,int> p2(policyMax - 1, 0);

            //Check if position is "populated"
            if(p1.first < pword.size())
            {
                p1.second = pword[p1.first] == theChar ? 1 : 0;
            }

            if(p2.first < pword.size())
            {
                p2.second = pword[p2.first] == theChar ? 1 : 0;
            }

            nrOfValidPasswords += ((p1.second + p2.second) == 1) ? 1 : 0;
        }

        it++;
    }
    return nrOfValidPasswords;
}

//================================================
vector<int> getIntegersInFile(const string& fName)
{
    ifstream infile(fName.c_str());
    string line;
    vector<int> v;

    while (getline(infile, line))
    {
        istringstream iss(line);
        int n;
        while (iss >> n)
        {
            v.push_back(n);
        }
    }

    return v;
}

vector<string> getLinesInFile(const string& fName)
{
    ifstream infile(fName.c_str());
    string line;
    vector<string> v;
    while (getline(infile, line))
    {
        v.push_back(line);
    }

    return v;
}
